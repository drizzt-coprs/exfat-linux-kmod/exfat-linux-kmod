# buildforkernels macro hint: when you build a new version or a new release
# that contains bugfixes or other improvements then you must disable the
# "buildforkernels newest" macro for just that build; immediately after
# queuing that build enable the macro again for subsequent builds; that way
# a new akmod package will only get build when a new one is actually needed
%if 0%{?fedora}
%global buildforkernels akmod
%global debug_package %{nil}
%endif

%define _pkgver  %(echo %{version} | sed 's/^\\([0-9]*\\.[0-9]*\\.[0-9]*\\)\\.\\([0-9]*\\)$/\\1-\\2arter97/')

Name:          exfat-linux-kmod
Version:       2.2.0.3
# Taken over by kmodtool
Release:       2%{?dist}
Summary:       exFAT filesystem module for Linux kernel
License:       GPLv2
URL:           https://github.com/arter97/exfat-linux/

Source0:       https://github.com/arter97/exfat-linux/archive/%{_pkgver}.tar.gz

Patch0:        0001-exfat-define-time_t-if-undefined.patch

# get the needed BuildRequires (in parts depending on what we build for)
%global AkmodsBuildRequires %{_bindir}/kmodtool
BuildRequires:  %{AkmodsBuildRequires}

%{!?kernels:BuildRequires: gcc, elfutils-libelf-devel }
# kmodtool does its magic here
%{expand:%(kmodtool --target %{_target_cpu} --repo rpmfusion --kmodname %{name} %{?buildforkernels:--%{buildforkernels}} %{?kernels:--for-kernels "%{?kernels}"} 2>/dev/null | grep -v kmod-common) }

%description
This exFAT filesystem module for Linux kernel is based on sdFAT drivers by Samsung,
which is used with their smartphone lineups.

The main objective of exfat-linux is to provide the best generic kernel drivers for exFAT.
That means Samsung-specific modifications such as fat12/16/32 handlings,
defrag and etc has been removed to make the code portable.

%prep
# error out if there was something wrong with kmodtool
%{?kmodtool_check}
# print kmodtool output for debugging purposes:
kmodtool  --target %{_target_cpu}  --repo rpmfusion --kmodname %{name} %{?buildforkernels:--%{buildforkernels}} %{?kernels:--for-kernels "%{?kernels}"} 2>/dev/null
%setup -q -c -T -a 0
pushd exfat-linux-%{_pkgver} >/dev/null
%autopatch -p1 -v
popd >/dev/null

for kernel_version  in %{?kernel_versions} ; do
    cp -a exfat-linux-%{_pkgver} _kmod_build_${kernel_version%%___*}
done

%build
for kernel_version in %{?kernel_versions}; do
    make %{?_smp_mflags} -C "${kernel_version##*___}" M=${PWD}/_kmod_build_${kernel_version%%___*} modules CONFIG_EXFAT_FS=m
done


%install
for kernel_version in %{?kernel_versions}; do
    mkdir -p  $RPM_BUILD_ROOT/%{kmodinstdir_prefix}/${kernel_version%%___*}/%{kmodinstdir_postfix}/
    install -D -m 0755 _kmod_build_${kernel_version%%___*}/exfat.ko \
        $RPM_BUILD_ROOT/%{kmodinstdir_prefix}/${kernel_version%%___*}/%{kmodinstdir_postfix}/
done
%{?akmod_install}



%changelog
* Thu Feb 27 2020 Timothy Redaelli <tredaelli@redhat.com> - 2.2.0.3-2
- Backport "exfat: define time_t if undefined"

* Fri Feb 07 2020 Timothy Redaelli <tredaelli@redhat.com> - 2.2.0.3-1
- Initial package
